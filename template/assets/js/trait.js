$('code').each((i, el) => {
    hljs.highlightElement(el);
});

function copyImplementationTemplate() {
    var code = $('#templateCodeBox').text();
    navigator.clipboard.writeText(code);
    $('#copyButton').text("✅");
    setTimeout(() => $('#copyButton').text("📋"), 1000);
}

function copyDerive() {
    $('#deriveCopyButton').text("✅");
    setTimeout(() => $('#deriveCopyButton').text("📋"), 1000);
}