$('code').each((i, el) => {
    hljs.highlightElement(el);
});

$.each(traitTree, (bagName,traitBag) => {
    let bagHTML = '<span class="bag"><label class="bagtag">' + bagName + '</label>';
    $.each(traitBag, (_,val) => {
        bagHTML += '<a class="home-list-item" href="' + val + '.html">' + val + '</a>';
    });
    bagHTML += '</span>';
    $("#cheatsheetlist").append(bagHTML);
});