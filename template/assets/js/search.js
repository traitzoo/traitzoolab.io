var searchEnabled = false;

$('#searchInput').bind('input', function() { 
    let text = $(this).val().toLowerCase();
    $("#searchlist").html("");
    if (text.length === 0) {
        searchEnabled = false;
        $("#eraseSearch").hide();
        $('#searcharea').fadeOut(200, () => {
            if (searchEnabled) return;
            $('#cheatsheet').fadeIn(200);
        });
    } else {
        searchEnabled = true;
        $.each(traitList, (_,val) => {
            if (val.toLowerCase().includes(text)) {
                $("#searchlist").append('<a class="list-item" href="' + val + '.html">' + val + '</a>');
            }
        });
        $("#eraseSearch").show();
        $('#cheatsheet').fadeOut(200, () => {
            if (!searchEnabled) return;
            $('#searcharea').fadeIn(200);
            let notempty = $("#searchlist").html().length !== 0;
            $("#searchlist").toggle(notempty);
            $("#not-found").toggle(!notempty);
        });
    }
});

$('#eraseSearch').on("click", () => {
    $('#searchInput').val("").trigger("input");
});