//! Safe text.
//!
//! Safe text is a wrapper around a formatted value, ready to be plugged in HTML.

use super::HTMLable;
use regex::Regex;

lazy_static! {
    /// Check for inline codes like `this`
    static ref INLINE_CODE_REGEX: Regex = Regex::new(r"`(?P<c>[^`]+)`").unwrap();
    /// Check for Markdown newlines (2 or more newlines)
    static ref NEWLINE_REGEX: Regex = Regex::new(r"(\r?\n|\r){2,}").unwrap();
}

/// An escaped text, verified and ready to be used in a template.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SafeText(String);
impl SafeText {
    /// Create a new, HTML compatible text out of a raw one. Lang is the programming
    /// language used for inline code.
    ///
    /// Escapes all HTML tags to make it compatible.
    ///
    /// # Errors
    /// Returns `None` if the raw text contains reserved characters
    /// (as of now, only `{{` or `}}` are reserved).
    pub fn new(raw: impl AsRef<str>, lang: impl AsRef<str>) -> Option<Self> {
        let raw = raw.as_ref();
        let lang = lang.as_ref();

        // {{ and }} are not allowed since they would interfer with template variables
        if raw.contains("{{") || raw.contains("}}") {
            None
        } else {
            // Escape HTML characters
            let raw = raw
                .replace('&', "&amp;")
                .replace('<', "&lt;")
                .replace('>', "&gt;")
                .replace('"', "&quot;")
                .replace('\'', "&#039;");

            // Replace MD inline code with its HTML counterpart
            let raw = INLINE_CODE_REGEX
                .replace_all(&raw, format!("<code class=\"language-{lang}\">$c</code>"))
                .to_string();
            let raw = NEWLINE_REGEX.replace_all(&raw, "<br/>").to_string();
            Some(Self(raw))
        }
    }
}

impl HTMLable for SafeText {
    fn output(self) -> String {
        self.0
    }
}
