//! Code block.
//!
//! `Codeblock` is a wrapper around a code block, which must then be displayed as is in a <code>
//! element.

use super::HTMLable;

/// An code block, ready to be used in a template.
#[derive(Debug, Clone)]
pub struct CodeBlock {
    /// Code inside the code block.
    content: String,
    /// Programming language of the block.
    lang: String,
}
impl CodeBlock {
    /// Create a new, HTML code block out of a raw one.
    ///
    /// # Errors
    /// Returns `None` if the raw text contains reserved characters
    /// (as of now, only `{{` or `}}` are reserved).
    pub fn new(content: impl AsRef<str>, lang: impl AsRef<str>) -> Option<Self> {
        let content = content.as_ref();

        // {{ and }} are not allowed since they would interfere with template variables
        if content.contains("{{") || content.contains("}}") {
            None
        } else {
            // Escape HTML characters
            let content = content
                .replace('&', "&amp;")
                .replace('<', "&lt;")
                .replace('>', "&gt;")
                .replace('"', "&quot;")
                .replace('\'', "&#039;");

            Some(Self {
                content,
                lang: lang.as_ref().to_owned(),
            })
        }
    }

    /// Gets the code content of the block.
    ///
    /// Prefer`output()` if you want to get the HTML output of the code block.
    pub fn content(&self) -> &str {
        &self.content
    }
}

impl HTMLable for CodeBlock {
    fn output(self) -> String {
        format!("<div id=\"templateCodeBoxWrapper\">
        <button id=\"copyButton\" onClick=\"copyImplementationTemplate()\">📋</button>
        <pre><code class=\"language-{}\" id=\"templateCodeBox\" style=\"margin-right:30px\">{}</code></pre>
    </div>", self.lang, self.content)
    }
}
