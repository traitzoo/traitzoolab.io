//! HTML generation utilities common to all cheatsheets
//!
//! This is mainly about `SafeText` (which escapes HTML), and template handling.

pub mod safetext;
pub use safetext::SafeText;
pub mod template;
pub use template::Template;
pub mod traitlist_js;
pub use traitlist_js::create_traitlist_file;
pub mod codeblock;
pub use codeblock::CodeBlock;

/// Can be converted into HTML.
pub trait HTMLable {
    /// Outputs the HTML content corresponding to self.
    fn output(self) -> String;
}

impl HTMLable for String {
    fn output(self) -> String {
        self
    }
}
