//! Traitlist generator.
//!
//! Generates the JS file containing a list of all the
//! traits in the website.

use super::{HTMLable, SafeText};
use std::collections::BTreeMap;
use std::fs;
use std::io::Write;

/// Creates a new file in the build, named `js/traitlist.js`,
/// which defines a global variable in javascript
/// with the list of the names of all the names given as input to this function.
pub fn create_traitlist_file(lang: impl AsRef<str>, trait_tree: BTreeMap<SafeText, Vec<SafeText>>) {
    let lang = lang.as_ref();

    // Compute sorted nested list of traits, taking grouping into account
    // Note: only 1 level of nesting/grouping is currently supported: every trait lives in a bag
    // which has a name. The tree is then a list of all bags.
    let traits_tree_html =
        trait_tree
            .clone()
            .into_iter()
            .fold(String::new(), |acc, (bagname, mut traits)| {
                let bagname = bagname.output();

                // Sort the traits in alphabetical order
                traits.sort();

                let traits_html = traits.into_iter().fold(String::new(), |acc, x| {
                    if acc.is_empty() {
                        format!("\"{}\"", x.output())
                    } else {
                        format!("{acc}, \"{}\"", x.output())
                    }
                });

                if acc.is_empty() {
                    format!("{bagname}: [{traits_html}]")
                } else {
                    format!("{acc}, {bagname}: [{traits_html}]")
                }
            });

    // Compute the sorted list of all traits, regardless of grouping
    let mut traits: Vec<SafeText> = trait_tree.into_iter().flat_map(|(_, x)| x).collect();
    traits.sort();
    let trait_list_html = traits.into_iter().fold(String::new(), |acc, x| {
        if acc.is_empty() {
            format!("\"{}\"", x.output())
        } else {
            format!("{acc}, \"{}\"", x.output())
        }
    });

    // Create directory
    fs::create_dir_all(format!("build/{lang}/js"))
        .unwrap_or_else(|_| panic!("Unable to create {lang} build directory"));

    // Create file
    let mut file = fs::File::create(format!("build/{lang}/js/traitlist.js"))
        .expect("Unable to create result file");
    writeln!(&mut file, "const traitList = [{trait_list_html}];").expect("Unable to write to file");
    writeln!(&mut file, "const traitTree = {{{traits_tree_html}}};")
        .expect("Unable to write to file");
}
