//! Template handling.

use super::HTMLable;
use std::fs;

/// Wrapper around a template file.
#[derive(Debug, Clone)]
pub struct Template(String);
impl Template {
    /// Create a new template from a file.
    ///
    /// # Errors
    /// Returns `None` if no such file exists.
    #[must_use]
    pub fn new_from_file(filename: impl AsRef<str>) -> Option<Self> {
        let content = fs::read_to_string(filename.as_ref()).ok()?;
        Some(Self(content))
    }

    /// Replaces a variable in the template by its corresponding value.
    ///
    /// # Warning
    /// The user must make *sure* that the value is available.
    ///
    /// # Panics
    /// Panics if the variable does not exist in the template.
    pub fn instantiate(&mut self, name: impl AsRef<str>, value: String) {
        let name = name.as_ref();
        let var = format!("{{{{{}}}}}", name);
        assert!(
            self.0.contains(&var),
            "Variable {} was not found in the template",
            var
        );
        self.0 = self.0.replace(&var, &value);
    }
}

impl HTMLable for Template {
    fn output(self) -> String {
        assert!(
            !self.0.contains("{{"),
            "Cannot output template: some fields were left uninstantiated"
        );
        self.0
    }
}
