//! Trait zoo static website generation.
//!
//! This crate turns the md files in the content folder into html pages in the build directory.
//!
//! The Trait Zoo is made up of several HTML pages, each one corresponding to a trait.
//! Each HTML page is made of several sections, which detail how the trait works.
//!
//! This crate has one file per HTML section, plus a file to create the final page out of a template.
//! There is also a page to parse the MD file which hold the content of the page, and a file (safetext)
//! to represent input values.
//!
//! The main function iters over the files of the content folder and turns each one into an HTML page.
#![warn(clippy::doc_markdown)]
#![warn(clippy::missing_errors_doc)]
#![warn(clippy::missing_panics_doc)]
#![warn(missing_docs)]
#![warn(clippy::missing_docs_in_private_items)]
#![warn(clippy::must_use_candidate)]

use fs_extra::dir::CopyOptions;

mod common;
mod haskell;
mod rust;

extern crate pest;
#[macro_use]
extern crate pest_derive;
#[macro_use]
extern crate lazy_static;

fn main() {
    // Create build directory (empty it if necessary)
    fs_extra::dir::create("build", true).expect("Unable to create rust build directory");

    // Load general assets into build directory
    let mut copy_options = CopyOptions::new();
    copy_options.content_only = true;
    fs_extra::dir::copy("template/assets", "build", &copy_options).expect("Could not load assets");

    // Build directories
    rust::build_dir();
    haskell::build_dir();
}
