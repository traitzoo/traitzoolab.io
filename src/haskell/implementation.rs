//! Implementation section generation.
//!
//! This section offers a sample implementation, that can be copied and pasted
//! into one's code.

use crate::common::{CodeBlock, HTMLable};

/// Builder of the Implementation section, which gives a list of optional and mandatory methods
/// inside the trait considered.
#[derive(Debug, Clone)]
pub struct Implementation {
    /// Code of a sample implementation.
    code: CodeBlock,
}

impl Implementation {
    /// Create a new section builder.
    pub fn new(code: CodeBlock) -> Self {
        Self { code }
    }
}

impl HTMLable for Implementation {
    fn output(self) -> String {
        self.code.output()
    }
}
