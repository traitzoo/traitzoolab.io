//! Requirements section generation.

use crate::common::{HTMLable, SafeText};

/// The Requirements section, which is a list of requirements.
#[derive(Debug, Clone)]
pub struct Requirements(Vec<SafeText>);
impl Requirements {
    /// Create a new page builder.
    #[must_use]
    pub fn new(list: Vec<SafeText>) -> Self {
        Self(list)
    }
}

impl HTMLable for Requirements {
    fn output(self) -> String {
        self.0
            .into_iter()
            .map(|r| {
                let r = r.output();
                format!("<a class=\"deps\" href=\"{}.html\">{}</a>", r, r)
            })
            .fold(String::new(), |acc, x| {
                if acc.is_empty() {
                    x
                } else {
                    acc + ", " + &x
                }
            })
    }
}
