//! Safetext with inline Haskell code
//!
//! It simplifies the use of the generic `SafeText` structure, by providing a function which
//! automatically chooses Rust as the default programming language for inline code.

use crate::common::SafeText;

/// Create a new, HTML compatible text out of a raw one.
///
/// Escapes all HTML tags to make it compatible.
///
/// # Errors
/// Returns `None` if the raw text contains reserved characters
/// (as of now, only `{{` or `}}` are reserved).
pub fn new(raw: impl AsRef<str>) -> Option<SafeText> {
    SafeText::new(raw, "haskell")
}
