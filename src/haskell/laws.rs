//! Laws section generation.

use crate::common::{HTMLable, SafeText};

/// Builder of the Laws section, which gives the list of laws the implementation must obey.
#[derive(Debug, Clone)]
pub struct Laws {
    /// List of laws
    laws: Vec<SafeText>,
}

impl Laws {
    /// Create a new laws section builder.
    #[must_use]
    pub fn new(laws: Vec<SafeText>) -> Self {
        Self { laws }
    }
}

impl HTMLable for Laws {
    fn output(self) -> String {
        self.laws
            .into_iter()
            .map(|law| format!("<li>{}</li>\n", law.output()))
            .collect::<String>()
    }
}
