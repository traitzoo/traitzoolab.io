//! Definition section generation.

use crate::common::{HTMLable, SafeText};

/// Builder of the Definition section, which gives a list of optional and mandatory methods inside
/// the trait considered.
#[derive(Debug, Clone)]
pub struct Definition {
    /// List of optional methods in the trait, ie methods that have a default implementation.
    optional: Vec<SafeText>,
    /// List of mandatory methods in the trait, ie methods that must be implemented.
    mandatory: Vec<SafeText>,
}

impl Definition {
    /// Create a new section builder.
    #[must_use]
    pub fn new(mandatory: Vec<SafeText>, optional: Vec<SafeText>) -> Self {
        Self {
            optional,
            mandatory,
        }
    }
}

impl HTMLable for Definition {
    fn output(self) -> String {
        self.mandatory
            .into_iter()
            .map(|d| {
                format!(
                    "<li class=\"li-required\"><code class=\"haskell\">{}</code></li>\n",
                    d.output()
                )
            })
            .collect::<String>()
            + &self
                .optional
                .into_iter()
                .map(|d| {
                    format!(
                        "<li class=\"li-optional\"><code class=\"haskell\">{}</code></li>\n",
                        d.output()
                    )
                })
                .collect::<String>()
    }
}
