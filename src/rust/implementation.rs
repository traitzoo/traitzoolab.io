//! Implementation section generation.
//!
//! This section offers a sample implementation, that can be copied and pasted
//! into one's code.

use crate::common::{CodeBlock, HTMLable};

/// Builder of the Implementation section, which gives a list of optional and mandatory methods
/// inside the trait considered.
#[derive(Debug, Clone)]
pub struct Implementation {
    /// Name of the trait.
    name: String,
    /// Whether the trait can be derived. Used to add an automatic block with derived implementation.
    can_be_derived: bool,
    /// Whether the trait is an auto trait. Used to add an automatic block of information.
    is_auto_trait: bool,
    /// Code of a sample implementation.
    code: CodeBlock,
}

impl Implementation {
    /// Create a new section builder.
    pub fn new(
        name: impl AsRef<str>,
        can_be_derived: bool,
        is_auto_trait: bool,
        code: CodeBlock,
    ) -> Self {
        Self {
            name: name.as_ref().to_owned(),
            can_be_derived,
            is_auto_trait,
            code,
        }
    }
}

impl HTMLable for Implementation {
    fn output(self) -> String {
        let mut info_block = String::new();
        let code = self.code.content();
        if self.can_be_derived {
            info_block += &format!("<div class=\"info-block\"><span class=\"info-block-icon\">🛈</span><span class=\"info-block-text\">Can be <a target=\"_blank\" href=\"https://doc.rust-lang.org/rust-by-example/trait/derive.html\">derived</a>.</span>
        <button id=\"deriveCopyButton\" onClick=\"navigator.clipboard.writeText('#[derive({})]'); copyDerive();\">📋</button></div>", self.name);
        }
        if self.is_auto_trait {
            info_block += "<div class=\"info-block\"><span class=\"info-block-icon\">🛈</span><span class=\"info-block-text\">This is an <a target=\"_blank\" href=\"https://stackoverflow.com/questions/49710852\">auto trait</a>, automatically implemented by the compiler on an opt-out basis.</span></div>";
        }
        if code.contains("unsafe") {
            info_block += "<div class=\"warning-block\"><span class=\"warning-block-icon\">⚠</span><span class=\"warning-block-text\">The following snippet contains unsafe code.</span></div>";
        }

        info_block + &self.code.output()
    }
}
