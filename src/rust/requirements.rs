//! Requirements section generation.

use super::safetext;
use crate::common::{HTMLable, SafeText};

use regex::Regex;

lazy_static! {
    /// Check for standard trait requirement.
    static ref STD_REQUIREMENT_REGEX: Regex = Regex::new(r"^(\w+)$").unwrap();
    /// Check for optional trait requirement.
    static ref OPT_REQUIREMENT_REGEX: Regex = Regex::new(r"^\?(\w+)$").unwrap();
    /// Check for negative trait requirement.
    static ref NEG_REQUIREMENT_REGEX: Regex = Regex::new(r"^!(\w+)$").unwrap();
}

/// A trait requirement (aka a supertrait).
#[derive(Clone, Debug)]
pub enum Requirement {
    /// Standard trait requirement: the trait is a supertrait of the one at hand.
    Standard(SafeText),
    /// Indicate that a given trait is optional.
    Optional(SafeText),
    /// Indicate that a given trait should not be implemented.
    Negative(SafeText),
}
impl Requirement {
    /// Create a new requirement.
    ///
    /// # Panics
    /// Panics if the trait requirement given as argument cannot be parsed.
    pub fn new(requirement: impl AsRef<str>) -> Self {
        let requirement = requirement.as_ref();

        if let Some(captures) = STD_REQUIREMENT_REGEX.captures(requirement) {
            Requirement::Standard(
                safetext::new(
                    captures
                        .get(1)
                        .expect("Regex must have at least a capture group for the name")
                        .as_str(),
                )
                .expect("Trait name is not HTML-friendly"),
            )
        } else if let Some(captures) = OPT_REQUIREMENT_REGEX.captures(requirement) {
            Requirement::Optional(
                safetext::new(
                    captures
                        .get(1)
                        .expect("Regex must have at least a capture group for the name")
                        .as_str(),
                )
                .expect("Trait name is not HTML-friendly"),
            )
        } else if let Some(captures) = NEG_REQUIREMENT_REGEX.captures(requirement) {
            Requirement::Negative(
                safetext::new(
                    captures
                        .get(1)
                        .expect("Regex must have at least a capture group for the name")
                        .as_str(),
                )
                .expect("Trait name is not HTML-friendly"),
            )
        } else {
            panic!("Badly formatted trait requirement: only accepted formats are `Sized`, `?Sized` or `!Sized`");
        }
    }
}

impl HTMLable for Requirement {
    fn output(self) -> String {
        match self {
            Requirement::Standard(name) => {
                let name = name.output();
                format!("<a class=\"deps\" href=\"{}.html\">{}</a>", name, name)
            }
            Requirement::Optional(name) => {
                let name = name.output();
                format!(
                    "<a class=\"deps info\" href=\"{}.html\">?{}</a>",
                    name, name
                )
            }
            Requirement::Negative(name) => {
                let name = name.output();
                format!(
                    "<a class=\"deps danger\" href=\"{}.html\">!{}</a>",
                    name, name
                )
            }
        }
    }
}

/// The Requirements section, which is a list of requirements.
#[derive(Debug, Clone)]
pub struct Requirements(Vec<Requirement>);
impl Requirements {
    /// Create a new page builder.
    #[must_use]
    pub fn new(list: Vec<Requirement>) -> Self {
        Self(list)
    }
}

impl HTMLable for Requirements {
    fn output(self) -> String {
        self.0
            .into_iter()
            .map(Requirement::output)
            .fold(String::new(), |acc, x| {
                if acc.is_empty() {
                    x
                } else {
                    acc + ", " + &x
                }
            })
    }
}
