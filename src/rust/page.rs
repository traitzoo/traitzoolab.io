//! Page generation.

use super::{Definition, Implementation, Requirements, Tldr};
use crate::common::{HTMLable, SafeText, Template};

/// Page builder. Contructs a whole page, based on the output of the other section builders.
#[derive(Debug, Clone)]
pub struct Page {
    /// Name of the trait.
    name: SafeText,
    /// Template used to build the page.
    template: Template,
    /// Summary section.
    tldr: Tldr,
    /// Requirements section.
    requirements: Requirements,
    /// Implementation section.
    implementation: Implementation,
    /// Definition section.
    definition: Definition,
    /// Link to the docs
    link_to_the_docs: SafeText,
}
impl Page {
    /// Create a new page builder.
    #[must_use]
    pub fn new(
        name: SafeText,
        template: Template,
        tldr: Tldr,
        requirements: Requirements,
        implementation: Implementation,
        definition: Definition,
        link_to_the_docs: SafeText,
    ) -> Self {
        Self {
            name,
            template,
            tldr,
            requirements,
            implementation,
            definition,
            link_to_the_docs,
        }
    }
}

impl HTMLable for Page {
    fn output(mut self) -> String {
        self.template.instantiate("name", self.name.output());
        self.template.instantiate("tldr", self.tldr.output());
        self.template
            .instantiate("requirements", self.requirements.output());
        self.template
            .instantiate("implementation", self.implementation.output());
        self.template
            .instantiate("definition", self.definition.output());
        self.template
            .instantiate("link", self.link_to_the_docs.output());
        self.template.output()
    }
}
