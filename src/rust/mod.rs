//! Rust content generation.
//!
//! The main function here is `build_dir()`, which builds the Rust directory.

use std::collections::BTreeMap;
use std::fs;
use std::io::Write;

mod parser;
use parser::parse;
mod page;
use page::Page;
mod requirements;
use requirements::{Requirement, Requirements};
mod tldr;
use tldr::Tldr;
mod definition;
use definition::Definition;
mod implementation;
use implementation::Implementation;
mod safetext;

use crate::common::*;
use fs_extra::dir::CopyOptions;

/// Builds the HTML files of the rust directory.
pub fn build_dir() {
    // Load template
    let template =
        Template::new_from_file("template/rust/template.html").expect("Unable to load template");

    // Create rust build directory (empty it if necessary)
    fs_extra::dir::create("build/rust", true).expect("Unable to create rust build directory");

    // Load assets into build directory
    let mut copy_options = CopyOptions::new();
    copy_options.content_only = true;
    fs_extra::dir::copy("template/rust/assets", "build/rust", &copy_options)
        .expect("Could not load rust assets");

    // We will remember the hierarchy and list of traits for the traitlist file.
    let mut trait_tree = BTreeMap::new();

    for bag in fs::read_dir("content/rust").expect("Cannot read `content` directory") {
        let bag = bag.expect("Cannot get entry in `content` folder");
        let filename = bag.file_name();
        let bagname = filename.to_str().expect("Could not get bag name");
        let bagname = SafeText::new(bagname, "rust").expect("Could not parse bag name");
        let bagpath = bag.path();
        assert!(
            bagpath.is_dir(),
            "Entries in content/rust must be directories"
        );

        let mut traitlist = vec![];
        for entry in fs::read_dir(bagpath).expect("Cannot read `content` directory") {
            let entry = entry.expect("Cannot get entry in `content` folder");
            let path = entry.path();
            assert!(
                !path.is_dir(),
                "Entries in the content/rust/{} must be files",
                bagname.output()
            );
            let name = path
                .as_path()
                .file_stem()
                .expect("Error getting the filename")
                .to_str()
                .expect("Error getting the filename")
                .to_owned();
            let contents =
                fs::read_to_string(path).expect("Something went wrong reading a file in `content`");
            let file = parse(&name, &contents);
            let template = template.clone();

            let tldr = Tldr::new(safetext::new(file.tldr()).unwrap());
            let requirements = Requirements::new(
                file.requirements()
                    .iter()
                    .map(|x| Requirement::new(&x))
                    .collect(),
            );
            let definition = Definition::new(
                file.definition()
                    .mandatory()
                    .iter()
                    .map(|x| safetext::new(&x).unwrap())
                    .collect(),
                file.definition()
                    .optional()
                    .iter()
                    .map(|x| safetext::new(&x).unwrap())
                    .collect(),
            );
            let implementation = Implementation::new(
                &name,
                file.can_be_derived(),
                file.is_auto_trait(),
                CodeBlock::new(file.implementation(), "rust").unwrap(),
            );
            let page = Page::new(
                safetext::new(&name).unwrap(),
                template,
                tldr,
                requirements,
                implementation,
                definition,
                safetext::new(&file.link_to_the_docs()).unwrap(),
            );

            let mut file = fs::File::create(format!("build/rust/{}.html", name))
                .expect("Unable to create result file");
            writeln!(&mut file, "{}", page.output()).expect("Unable to write to file");
            traitlist.push(safetext::new(name).unwrap());
        }

        trait_tree.insert(bagname, traitlist);
    }

    // Create remaining helper files.
    create_traitlist_file("rust", trait_tree);
}
