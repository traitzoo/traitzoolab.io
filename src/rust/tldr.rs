//! Tl;dr generation.

use crate::common::{HTMLable, SafeText};

/// The TL;DR section.
#[derive(Debug, Clone)]
pub struct Tldr(SafeText);
impl Tldr {
    /// Create a new TL;DR builder.
    #[must_use]
    pub fn new(content: SafeText) -> Self {
        Self(content)
    }
}

impl HTMLable for Tldr {
    fn output(self) -> String {
        self.0.output()
    }
}
