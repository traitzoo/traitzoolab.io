//! Parsing the Markdown file.
//!
//! MD files must have a specific format for the parser to accept it.
//! This is the main framework for MD files:
//!
//! ```markdown
//! # TL;DR
//!
//! # Requirements
//!
//! # Definition
//!
//! # Implementation
//!
//! Template implementation here.
//! ```
//!
//! Details can be found in the pest grammar file.

use pest::Parser;

/// Pest-derived parser of a MD file.
#[derive(Parser)]
#[grammar = "rust/grammar.pest"]
struct MdParser;

/// A definition in the MD file, made of mandatory and optional items.
#[derive(Debug, Clone, Default)]
pub struct Definition {
    /// List of mandatory items in the definition.
    mandatory: Vec<String>,
    /// List of optional items in the definition.
    optional: Vec<String>,
}
impl Definition {
    /// Gets the list of mandatory items.
    pub fn mandatory(&self) -> &Vec<String> {
        &self.mandatory
    }

    /// Gets the list of optional items.
    pub fn optional(&self) -> &Vec<String> {
        &self.optional
    }
}

/// The representation of a MD file after it has been parsed
/// by our in-house parser. All sections have been extracted
/// and formatted in a useful manner, ready to be plugged in
/// the template.
pub struct MdFile {
    /// The TD;DR section.
    tldr: String,
    /// The implementaiton section.
    implementation: String,
    /// The requirements section.
    requirements: Vec<String>,
    /// The definition section.
    definition: Definition,
    /// Can the trait be derived.
    can_be_derived: bool,
    /// Is it an auto trait.
    is_auto_trait: bool,
    /// Link to the docs.
    link: String,
}
impl MdFile {
    /// Gets the TL;DR section out of the md file.
    pub fn tldr(&self) -> &String {
        &self.tldr
    }

    /// Gets the implementation section out of the md file.
    pub fn implementation(&self) -> &String {
        &self.implementation
    }

    /// Gets the implementation section out of the md file.
    pub fn requirements(&self) -> &Vec<String> {
        &self.requirements
    }

    /// Gets the definition section out of the md file.
    pub fn definition(&self) -> &Definition {
        &self.definition
    }

    /// Acessor to the `can_be_derived` field.
    pub fn can_be_derived(&self) -> bool {
        self.can_be_derived
    }

    /// Acessor to the `is_auto_trat` field.
    pub fn is_auto_trait(&self) -> bool {
        self.is_auto_trait
    }

    /// Link to the docs
    pub fn link_to_the_docs(&self) -> &String {
        &self.link
    }
}

/// Parses the `contents` of a file `name` and returns an `MdFile` on success.
///
/// # Panics
/// Panics if the contents are not well formatted.
#[must_use]
pub fn parse(name: impl AsRef<str>, contents: impl AsRef<str>) -> MdFile {
    let mut pairs = MdParser::parse(Rule::md_file, contents.as_ref())
        .unwrap_or_else(|e| panic!("{}{}", name.as_ref(), e));
    let file = pairs.next().unwrap();

    let mut tldr = None;
    let mut implementation = None;
    let mut requirements = None;
    let mut can_be_derived = false;
    let mut is_auto_trait = false;
    let mut definition = None;
    let mut link = None;

    for pair in file.into_inner() {
        match pair.as_rule() {
            Rule::EOI => (),
            Rule::tldr => {
                assert!(
                    tldr.is_none(),
                    "There is two TLDR sections in {}",
                    name.as_ref()
                );
                let content = pair.into_inner().next().unwrap();
                assert!(content.as_rule() == Rule::md_block);
                tldr = Some(content.as_str().to_owned());
            }
            Rule::implementation => {
                assert!(
                    implementation.is_none(),
                    "There is two Implementation sections in {}",
                    name.as_ref()
                );
                let mut pairs = pair.into_inner();
                let mut pair = pairs.next().unwrap();
                while pair.as_rule() == Rule::notes {
                    let inner = pair.into_inner().next().unwrap();
                    match inner.as_rule() {
                        Rule::can_be_derived => {
                            assert!(
                                !can_be_derived,
                                "Can be derived block has been enabled at least twice"
                            );
                            can_be_derived = true;
                        }
                        Rule::is_auto_trait => {
                            assert!(
                                !is_auto_trait,
                                "Is auto trait block has been enabled at least twice"
                            );
                            is_auto_trait = true;
                        }
                        rule => panic!("Unexpected info block rule {rule:?}"),
                    }
                    pair = pairs.next().unwrap();
                }
                assert!(pair.as_rule() == Rule::md_code);
                let code = pair.into_inner().next().unwrap();
                assert!(code.as_rule() == Rule::code);
                implementation = Some(code.as_str().to_owned());
            }
            Rule::requirements => {
                assert!(
                    requirements.is_none(),
                    "There is two Requirements sections in {}",
                    name.as_ref()
                );
                let contents = pair.into_inner();
                requirements = Some(contents.map(|r| r.as_str().to_owned()).collect());
            }
            Rule::definition => {
                assert!(
                    definition.is_none(),
                    "There is two Definition sections in {}",
                    name.as_ref()
                );
                let contents = pair.into_inner();
                let mut res = Definition::default();
                for item in contents {
                    match item.as_rule() {
                        Rule::mandatory_definition_item => {
                            let item = item.into_inner().next().unwrap();
                            res.mandatory.push(item.as_str().to_owned());
                        }
                        Rule::optional_definition_item => {
                            let item = item.into_inner().next().unwrap();
                            res.optional.push(item.as_str().to_owned());
                        }
                        x => panic!("Unexpected grammar rule here {x:?}"),
                    }
                }
                definition = Some(res);
            }
            Rule::link_to_the_docs => {
                assert!(
                    link.is_none(),
                    "There is two links to the docs in {}",
                    name.as_ref()
                );
                link = Some(pair.as_str().to_string());
            }
            x => panic!("Unexpected rule {x:?}"),
        }
    }

    MdFile {
        tldr: tldr.expect("No tldr found"),
        implementation: implementation.expect("No implementation found"),
        requirements: requirements.expect("No requirements found"),
        definition: definition.expect("No definition found"),
        can_be_derived,
        is_auto_trait,
        link: link.expect("No link to the docs found"),
    }
}
