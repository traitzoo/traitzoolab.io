# Foreword

It is highly recommanded to read at least the `README` before going any further.

# I want to add a new trait/typeclass

Go in the `content/{language}` directory. Open the folder whose name is the group/bag to which the new typeclass/trait belongs. If there is none, create a new folder here with a new name of a grouping and go in that folder. Create a new `Yourtrait.md` file in the relevant folder and add the contents of the cheatsheet inside (you can follow the example of other files in the folder to know how to write it, or see the README for details). Run `cargo run` and check that the file in the `build` directory exist and is displayed as expected.

# I want to add a new section
Eveyrthing takes place in `src/{language}`. Create new rules in the `grammar.pest` and update `parser.rs` accordingly to take into account this new section. Create a new file in the `src/{language}` folder based on the contents of another HTML section generator (`implementation.rs` for instance). Your generator should implement `HTMLable`. Create your generator in the `mod.rs` and use it in `page.rs` to output your section.
