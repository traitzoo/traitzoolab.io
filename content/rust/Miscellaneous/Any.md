https://doc.rust-lang.org/std/any/trait.Any.html

# TL;DR

The trait for dynamic typing. Use an `Any` trait object to emulate a value of a dynamic type.

# Requirements

# Definition

+ fn type_id(&self) -> TypeId;

# Implementation

Auto trait.

```rust
/** Already implemented for any T: 'static + ?Sized **/
```