https://doc.rust-lang.org/std/default/trait.Default.html

# TL;DR

Has a default instance.

# Requirements

# Definition

+ fn default() -> Self;

# Implementation

```rust
impl Default for usize {
    fn default() -> Self {
        0
    }
}
```