https://doc.rust-lang.org/std/cmp/trait.Eq.html

# TL;DR

Implementation of the `==` operator, which must be reflexive, symmetric and transitive.

# Requirements

PartialEq

# Definition

# Implementation

Can be derived.

```rust
impl Eq for MyStruct {  }
```