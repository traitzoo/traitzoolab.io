https://doc.rust-lang.org/std/cmp/trait.Ord.html

# TL;DR

Can be compared. Forms a total order. `cmp()` must be consistent with `PartialEq`.

# Requirements

Eq, PartialOrd

# Definition

+ fn cmp(&self, other: &Self) -> Ordering;
* fn max(self, other: Self) -> Self;
* fn min(self, other: Self) -> Self;
* fn clamp(self, min: Self, max: Self) -> Self;

# Implementation

Can be derived.

```rust
impl Ord for MyStruct {
    fn cmp(&self, other: &Self) -> Ordering {
        self.field.cmp(&other.field)
    }
}
```