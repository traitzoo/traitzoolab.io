https://doc.rust-lang.org/std/cmp/trait.PartialOrd.html

# TL;DR

Can be compared. Forms a partial order. If you implement all the methods, they must
be consistent with each other and with `PartialEq`. Rk: if it is `Ord` too, you might want to use 
`cmp()` while implementing `partial_cmp()`. Rhs can be `?Sized`.

# Requirements

PartialEq

# Definition

+ fn partial_cmp(&self, other: &Rhs) -> Option<Ordering>;
* fn lt(&self, other: &Rhs) -> bool;
* fn le(&self, other: &Rhs) -> bool;
* fn ge(&self, other: &Rhs) -> bool;

# Implementation

Can be derived.

```rust
impl PartialOrd for MyStruct {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.field.partial_cmp(&other.field)
    }
}
```