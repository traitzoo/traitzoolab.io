https://doc.rust-lang.org/std/cmp/trait.PartialEq.html

# TL;DR

Implementation of the `==` operator. If hand-written, `!=` implementation must be coherent with the `==` one. Note: Rhs can be `?Sized`.

# Requirements

# Definition

+ fn eq(&self, other: &Rhs) -> bool;
* fn ne(&self, other: &Rhs) -> bool;

# Implementation

Can be derived.

```rust
impl PartialEq for MyStruct {
    fn eq(&self, other: &Self) -> bool {
        todo!()
    }
}
```