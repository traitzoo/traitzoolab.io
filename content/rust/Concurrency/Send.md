https://doc.rust-lang.org/std/marker/trait.Send.html

# TL;DR

Safe to send to another thread.

# Requirements

# Definition

# Implementation

Auto trait.

```rust
unsafe impl Send for MyStruct {}
```
