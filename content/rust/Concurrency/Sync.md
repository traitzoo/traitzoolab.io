https://doc.rust-lang.org/std/marker/trait.Sync.html

# TL;DR

Safe to share between threads.  A type `T` is Sync iff its reference `&T` is `Send`.

# Requirements

# Definition

# Implementation

Auto trait.

```rust
unsafe impl Sync for MyStruct {}
```
