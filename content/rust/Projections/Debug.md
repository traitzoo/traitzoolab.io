https://doc.rust-lang.org/std/fmt/trait.Debug.html

# TL;DR

Implements Debug formatting. Can use `{:?}` with it inside macros like `println!`.

# Requirements

# Definition

+ fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error>;

# Implementation

Can be derived.

```rust
use core::fmt;
impl fmt::Debug for MyStruct {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Point")
         .field("x", &self.x)
         .field("y", &self.y)
         .finish()
    }
}
```