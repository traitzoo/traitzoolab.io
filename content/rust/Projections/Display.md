https://doc.rust-lang.org/std/fmt/trait.Display.html

# TL;DR

Can be displayed, i.e. can be formatted using `{}` in macros like `println!`.

# Requirements

# Definition

+ fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error>;

# Implementation

```rust
use std::fmt;

impl fmt::Display for MyStruct {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.my_field)
    }
}
```