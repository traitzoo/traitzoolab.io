https://doc.rust-lang.org/std/ops/trait.IndexMut.html

# TL;DR

Can be indexed (`[]`) with an instance of type parameter `Idx`, and the indexed content is mutable.

# Requirements

Index

# Definition

+ fn index_mut(&mut self, index: Idx) -> &mut Self::Output;

# Implementation

```rust
impl IndexMut<MyIdx> for MyStruct {
    fn index_mut(&mut self, index: MyIdx) -> &mut Self::Output {
        todo!()
    }
}
```