https://doc.rust-lang.org/std/ops/trait.Index.html

# TL;DR

Can be indexed (`[]`) with an instance of type parameter `Idx`.

# Requirements

# Definition

+ type Output: ?Sized;
+ fn index(&self, index: Idx) -> &Self::Output;

# Implementation

```rust
impl Index<MyIdx> for MyStruct {
    type Output = MyOutput;

    fn index(&self, index: MyIdx) -> &Self::Output {
        todo!()
    }
}
```