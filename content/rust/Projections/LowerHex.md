https://doc.rust-lang.org/std/fmt/trait.LowerHex.html

# TL;DR

Implements (lower) hexadecimal formatting. Can use `{:x}`/`{:#x}` with it inside macros like `println!`.

# Requirements

# Definition

+ fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error>;

# Implementation

```rust
use std::fmt;
impl fmt::LowerHex for MyStruct {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:#x}", self.my_field)
    }
}
```