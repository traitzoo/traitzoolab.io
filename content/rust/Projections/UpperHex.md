https://doc.rust-lang.org/std/fmt/trait.UpperHex.html

# TL;DR

Implements (upper) hexadecimal formatting. Can use `{:X}`/`{:#X}` with it inside macros like `println!`.

# Requirements

# Definition

+ fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error>;

# Implementation

```rust
use std::fmt;
impl fmt::UpperHex for MyStruct {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:#x}", self.my_field)
    }
}
```