https://doc.rust-lang.org/std/hash/trait.Hash.html

# TL;DR

Can compute a hash of it. Remark: if `a==b` then `hash(a)==hash(b)`.

# Requirements

# Definition

+ fn hash(&self, state: &mut impl Hasher);
* fn hash_slice(data: &[Self], state: &mut impl Hasher);

# Implementation

Can be derived.

```rust
impl Hash for MyStruct {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
        self.phone.hash(state);
    }
}
```