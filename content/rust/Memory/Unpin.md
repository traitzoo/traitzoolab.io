https://doc.rust-lang.org/std/marker/trait.Unpin.html

# TL;DR

Can be moved after it has been pinned. Essentially a type whose content does not depend on where it's located in memory.

# Requirements

# Definition

# Implementation

Auto trait.

```rust
impl Unpin for MyStruct {}
```