https://doc.rust-lang.org/core/marker/trait.Sized.html

# TL;DR

Has a constant size known at compile time.

# Requirements

# Definition

# Implementation

Auto trait.

```rust
/// Not to be implemented manually
```