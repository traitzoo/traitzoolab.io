https://doc.rust-lang.org/std/clone/trait.Clone.html

# TL;DR

Can be cloned (can produce a clone of a value out of a reference). Cloning is explicit and can be costly, contrary to copying.

# Requirements

# Definition

+ fn clone(&self) -> Self;
* fn clone_from(&mut self, source: &Self);

# Implementation

Can be derived.

```rust
impl Clone for MyStruct {
    fn clone(&self) -> Self {
        todo!()
    }
}
```