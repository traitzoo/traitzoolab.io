https://doc.rust-lang.org/std/ops/trait.Drop.html

# TL;DR

Has a manually-defined custom destructor.

# Requirements

# Definition

+ fn drop(&mut self);

# Implementation

```rust
impl Drop for MyStruct {
    fn drop(&mut self) {
        todo!()
    }
}
```