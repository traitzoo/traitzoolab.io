https://doc.rust-lang.org/std/iter/trait.IntoIterator.html

# TL;DR

Can be transformed into an iterator, whose underlying `Item` will be `Self::Item`.

# Requirements

# Definition

+ type Item;
+ type IntoIter where <Self::IntoIter as Iterator>::Item == Self::Item; // i.e. which implements Iterator and  whose underlying Item are the same as my Self::Item
+ fn into_iter(self) -> Self::IntoIter;

# Implementation

```rust
impl IntoIterator for MyStruct {
    type Item = MyItem;
    type IntoIter = MyIter;

    fn into_iter(self) -> Self::IntoIter {
        todo!()
    }
}
```