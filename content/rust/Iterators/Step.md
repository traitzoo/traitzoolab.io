https://doc.rust-lang.org/core/iter/trait.Step.html

# TL;DR

Can be stepped backward and forward by some `usize` value.

# Requirements

Clone, PartialOrd, Sized

# Definition

+ fn steps_between(start: &Self, end: &Self) -> Option<usize>;
+ fn forward_checked(start: Self, count: usize) -> Option<Self>;
+ fn backward_checked(start: Self, count: usize) -> Option<Self>;
* fn forward(start: Self, count: usize) -> Self { ... }
* unsafe fn forward_unchecked(start: Self, count: usize) -> Self { ... }
* fn backward(start: Self, count: usize) -> Self { ... }
* unsafe fn backward_unchecked(start: Self, count: usize) -> Self { ... }

# Implementation

```rust
impl Step for i32 {
    fn steps_between(start: &Self, end: &Self) -> Option<usize> {
        if *start <= *end {
            Some((*end - *start) as usize)
        } else {
            None
        }
    }

    fn forward_checked(start: Self, n: usize) -> Option<Self> {
        match Self::try_from(n) {
            Ok(n) => start.checked_add(n),
            Err(_) => None, // if n is out of range, unsigned_start + n is too
        }
    }

    fn backward_checked(start: Self, n: usize) -> Option<Self> {
        match Self::try_from(n) {
            Ok(n) => start.checked_sub(n),
            Err(_) => None, // if n is out of range, unsigned_start - n is too
        }
    }
}
```