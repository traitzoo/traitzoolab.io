https://doc.rust-lang.org/std/convert/trait.From.html

# TL;DR

Provides conversion from another type into this type (dual of `Into`). `A: From<B>` iff `B: Into<A>`.
 Prefer to use `Into` in trait bounds or function arguments.

# Requirements

# Definition

+ fn from(T) -> Self;

# Implementation

```rust
impl From<OtherStruct> for MyStruct {
    fn from(other: OtherStruct) -> Self {
        todo!()
    }
}
```