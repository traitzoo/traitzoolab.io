https://doc.rust-lang.org/std/ops/trait.Deref.html

# TL;DR

Can be dereferenced via the * operator. Associate type `Target` is the target dereferenced type.

# Requirements

# Definition

+ type Target: ?Sized;
+ fn deref(&self) -> &Self::Target;

# Implementation

```rust
impl Deref for MyStruct {
    type Target = OtherStruct;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}
```