https://doc.rust-lang.org/std/convert/trait.Into.html

# TL;DR

Provides conversion that can possibly fail (which case it returns an `Err`) from this type
into another type (dual of `TryFrom`). `A: TryFrom<B>` iff `B: TryInto<A>`. Prefer to implement
`TryFrom`.

# Requirements

# Definition

+ fn try_into(self) -> Result<T, Self::Error>;

# Implementation

```rust
impl TryInto<OtherStruct> for MyStruct {
    type Error = MyError;

    fn try_into(self) -> Result<T, Self::Error> {
        todo!()
    }
}
```