https://doc.rust-lang.org/std/convert/trait.TryFrom.html

# TL;DR

Provides that can possibly fail (which case it returns an `Err`) conversion from another type into
this type (dual of `TryInto`). `A: TryFrom<B>` iff `B: TryInto<A>`. Prefer to use `TryInto` in trait
bounds or function arguments.

# Requirements

# Definition

+ fn try_from(value: T) -> Result<Self, Self::Error>;

# Implementation

```rust
impl TryFrom<OtherStruct> for MyStruct {
    type Error = MyError;

    fn try_from(other: OtherStruct) -> Result<Self, Self::Error> {
        todo!()
    }
}
```