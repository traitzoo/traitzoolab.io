https://doc.rust-lang.org/std/convert/trait.Into.html

# TL;DR

Provides conversion from this type into another type (dual of `From`). `A: From<B>` iff `B: Into<A>`.
 Prefer to implement `From`.

# Requirements

# Definition

+ fn into(self) -> T;

# Implementation

```rust
impl Into<OtherStruct> for MyStruct {
    fn into(self) -> OtherStruct {
        todo!()
    }
}
```