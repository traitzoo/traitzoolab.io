https://doc.rust-lang.org/std/convert/trait.AsMut.html

# TL;DR

Mutable references of it can be treated as mutable references of another type (with low conversion overhead).

# Requirements

# Definition

+ fn as_mut(&mut self) -> &mut T;

# Implementation

```rust
impl AsMut<[u8]> for Vec<u8> {
    fn as_mut(&mut self) -> &mut [u8] {
        self.as_mut_slice()
    }
}
```