https://doc.rust-lang.org/std/convert/trait.AsRef.html

# TL;DR

References of it can be treated as references of another type (with low conversion overhead).

# Requirements

?Sized

# Definition

+ fn as_ref(&self) -> &T;

# Implementation

```rust
impl AsRef<[u8]> for str {
    fn as_ref(&self) -> &[u8] {
        self.as_bytes()
    }
}
```