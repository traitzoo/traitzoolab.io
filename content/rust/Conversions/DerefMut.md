https://doc.rust-lang.org/std/ops/trait.DerefMut.html

# TL;DR

Can be dereferenced via the * operator, and the target type can be mutated.

# Requirements

Deref

# Definition

+ fn deref_mut(&mut self) -> &mut Self::Target;

# Implementation

```rust
impl DerefMut for MyStruct {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.value
    }
}
```