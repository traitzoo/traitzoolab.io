https://doc.rust-lang.org/std/ops/trait.Add.html

# TL;DR

Implements the + operator. The generic parameter is the other operand type.

# Requirements

# Definition

+ type Output;
+ fn add(self, rhs: Rhs) -> Self::Output;

# Implementation

```rust
impl Add for MyStruct {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        todo!()
    }
}
```
