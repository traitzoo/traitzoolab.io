https://doc.rust-lang.org/std/ops/trait.BitAnd.html

# TL;DR

Implements the & (bitwise and) operator. The generic parameter T is the other operand type.

# Requirements

# Definition

+ type Output;
+ fn bitand(self, rhs: Rhs) -> Self::Output;

# Implementation

```rust
impl BitAnd<CustomMask> for MyAddress {
    type Output = MyAddress;

    fn bitand(self, mask: CustomMask) -> Self::Output {
        todo!()
    }
}
```