https://doc.rust-lang.org/std/ops/trait.Div.html

# TL;DR

Implements the / operator. The generic T is the other operand type.

# Requirements

# Definition

+ type Output;
+ fn div(self, rhs: Rhs) -> Self::Output;

# Implementation

```rust
impl Div for MyStruct {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        todo!()
    }
}
```