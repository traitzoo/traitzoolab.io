https://doc.rust-lang.org/std/ops/trait.Sub.html

# TL;DR

Implements the - operator. The generic parameter is the other operand type.

# Requirements

# Definition

+ type Output;
+ fn sub(self, rhs: Rhs) -> Self::Output;

# Implementation

```rust
impl Sub for MyStruct {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        self.0 - other.0
    }
}
```