https://doc.rust-lang.org/std/ops/trait.BitOr.html

# TL;DR

Implements the | (bitwise or) operator. The generic parameter T is the other operand type.

# Requirements

# Definition

+ type Output;
+ fn bitor(self, rhs: Rhs) -> Self::Output;

# Implementation

```rust
impl BitOr<CustomMask> for MyAddress {
    type Output = MyAddress;

    fn bitor(self, mask: CustomMask) -> Self::Output {
        todo!()
    }
}
```