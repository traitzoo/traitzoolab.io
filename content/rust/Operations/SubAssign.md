https://doc.rust-lang.org/std/ops/trait.SubAssign.html

# TL;DR

Implements the -= operator. The generic parameter is the other operand type.

# Requirements

# Definition

+ fn sub_assign(&mut self, rhs: Rhs);

# Implementation

```rust
impl SubAssign<usize> for Point {
    fn sub_assign(&mut self, offset: usize) {
        *self = Self {
            x: self.x - offset,
            y: self.y - offset,
        };
    }
}
```
