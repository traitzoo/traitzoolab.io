https://doc.rust-lang.org/std/ops/trait.AddAssign.html

# TL;DR

Implements the += operator. The generic parameter is the other operand type.

# Requirements

# Definition

+ fn add_assign(&mut self, rhs: Rhs);

# Implementation

```rust
impl AddAssign<usize> for Point {
    fn add_assign(&mut self, offset: usize) {
        *self = Self {
            x: self.x + offset,
            y: self.y + offset,
        };
    }
}
```
