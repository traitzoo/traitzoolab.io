https://doc.rust-lang.org/std/ops/trait.Mul.html

# TL;DR

Implements the * operator. The generic T is the other operand type.

# Requirements

# Definition

+ type Output;
+ fn mul(self, rhs: Rhs) -> Self::Output;

# Implementation

```rust
impl Mul for MyStruct {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        todo!()
    }
}
```
