https://doc.rust-lang.org/std/ops/trait.Rem.html

# TL;DR

Implements the % operator. The generic parameter T is the other operand type.

# Requirements

# Definition

+ type Output;
+ fn rem(self, rhs: Rhs) -> Self::Output;

# Implementation

```rust
impl Rem<usize> for Apples {
    type Output = Apples;

    fn rem(self, modulus: usize) -> Self::Output {
        todo!()
    }
}
```