https://doc.rust-lang.org/std/future/trait.Future.html

# TL;DR

Promise to return an instance of type `Output` in the future. This stands for an (possibly) unfinished computation,
which can be polled for completion. When it completes, `poll()` returns `Ready(output)`, otherwise it
returns `Pending` (which case you need to poll later again).

All `async` functions actually return `Future`s whose `Output` is the displayed return type of the `async`.

# Requirements

# Definition

+ type Output;
+ fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output>;

# Implementation

```rust
impl Future for MyStruct {
    type Output = MyOutput;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        Poll::Pending
    }
}
```
