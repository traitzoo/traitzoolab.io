# TL;DR

Profunctor which is compatible with the exponential. Related to the grate optic.

# Requirements

Profunctor

# Definition

+ closed :: p a b -> p (c -> a) (c -> b) 

# Laws

# Implementation

```haskell
instance Closed (->) where
  closed pab = \ctoa -> pab . ctoa
```