# TL;DR

Generalization of a relationship between two types `a` and `b`. Technically speaking,
this is a functor which is contravariant in its first argument and covariant in its second one.

# Requirements

# Definition

+ dimap :: (c -> a) -> (b -> d) -> p a b -> p c d
* lmap :: (c -> a) -> p a b -> p c b
* rmap :: (b -> d) -> p a b -> p a d

# Laws

* Identity: `dimap id id = id`

# Implementation

```haskell
instance Profunctor (->) where
  dimap f g pab = g . pab . f
```