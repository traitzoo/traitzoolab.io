# TL;DR

Profunctor which is compatible with the disjoint union. You can implement either `left'` or `right'`. Related to the prism optic.

# Requirements

Profunctor

# Definition

+ left' :: p a b -> p (Either a c) (Either b c)
* right' :: p a b -> p (Either c a) (Either c b)

# Laws

# Implementation

```haskell
instance Choice (->) where
  left' pab (Left a)  = Left $ pab a
  left' pab (Right c) = Right c
```