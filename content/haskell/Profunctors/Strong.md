# TL;DR

Profunctor which is compatible with the type product. You can implement either `first'` or `second'`. Related to the lens optic.

# Requirements

Profunctor

# Definition

+ first' :: p a b -> p (a, c) (b, c)
* second' :: p a b -> p (c, a) (c, b)

# Laws

# Implementation

```haskell
instance Strong (->) where
  first' pab (a, c) = (pab a, c)
```