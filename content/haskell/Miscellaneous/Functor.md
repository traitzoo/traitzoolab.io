# TL;DR

Type wrapper/box `f` s.t. you can operate on the contents of a boxed value to produce another boxed value.

# Requirements

# Definition

+ fmap :: (a -> b) -> f a -> f b

# Laws

* Identity: `fmap id = id`
* Composition: `fmap (g . h) = (fmap g) . (fmap h)`

# Implementation

```haskell
instance Functor [] where
  fmap _ []     = []
  fmap g (x:xs) = g x : fmap g xs
```