# TL;DR

Type wrapper/box `f` s.t. you can operate on the contents of a boxed value to produce another boxed value, produce a boxed value out of any value, and apply boxed functions to boxed values.

# Requirements

Functor

# Definition

+ pure :: a -> f a
+ (<*>) :: f (a -> b) -> f a -> f b
* liftA2 :: (a -> b -> c) -> f a -> f b -> f c
* (*>) :: f a -> f b -> f b
* (<*) :: f a -> f b -> f a

# Laws

* Identity: `pure id <*> v = v`
* Homomorphism: `pure f <*> pure x = pure (f x)`
* Interchange: `u <*> pure y = pure ($ y) <*> u`
* Composition: `u <*> (v <*> w) = pure (.) <*> u <*> v <*> w`

# Implementation

```haskell
instance Applicative [] where
  pure x = [x]
  gs <*> xs = [ g x | g <- gs, x <- xs ]
```