# TL;DR

Type wrapper/box `f` made of two compartments s.t. you can operate on the contents of each
compartment separately. You must provide `bimap` or (`first` and `second`).

# Requirements

# Definition

+ bimap :: (a -> b) -> (c -> d) -> p a c -> p b d
* first :: (a -> b) -> p a c -> p b c
* second :: (b -> c) -> p a b -> p a c

# Laws

* Identity: `bimap id id = id`
* Identify for first (if provided): `first id = id`
* Identity for second (if provided): `second id = id`
* Compatibility: `bimap f g = first f . second g`

# Implementation

```haskell
instance Bifunctor (,) where
  bimap ab cd (a,c) = (ab a, cd c)
```