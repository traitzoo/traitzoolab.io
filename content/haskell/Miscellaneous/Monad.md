# TL;DR

Type wrapper/box `f` s.t. you can operate on the contents of a boxed value to produce another boxed value, and produce a boxed value out of any value. You can also reduce nested boxes (`f (...) f a`s) to a single box (`f a`).

# Requirements

Applicative

# Definition

+ (>>=)  :: m a -> (a -> m b) -> m b
* (>>)   :: m a -> m b -> m b
* return :: a -> m a

# Laws

* Left identity: `return >=> g = g`
* Right identity: `g >=> return = g`
* Associativity: `(g >=> h) >=> k = g >=> (h >=> k)`

# Implementation

```haskell
instance Monad [] where
    xs >>= f = [y | x <- xs, y <- f x]
```