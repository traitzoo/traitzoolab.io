# TL;DR

Algebraic structure with a unit and a way to combine elements.

# Requirements

# Definition

+ mempty :: m
+ (<>) :: m -> m -> m

# Laws

* Left identity: `mempty <> x = x`
* Right identity: `x <> mempty = x`
* Associativity: `(x <> y) <> z = x <> (y <> z)`

# Implementation

```haskell
instance Monoid [] where
  mempty = []
  (h:t) <> other = h:(t <> other)
```