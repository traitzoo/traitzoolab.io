# The Trait Zoo

The Trait Zoo is a collection of cheatsheets about Rust traits (and Haskell typeclasses), all in a single static website deployed to [https://traitzoo.gitlab.io/](this address).

# How to compile

You will need a Rust stable compiler. To compile, just clone this repository and launch `cargo run` from within the root of the project. You should see a new folder named `build` if everything went right. This folder contains all the website, and is ready to be deployed.

# Contributing

If you want to contribute, that's really easy: go and see the CONTRIBUTING file where everything is explained.

# Rust Cheatsheet template

```markdown
https:://linktothedocs

# TL;DR

Quick summary.

# Requirements

{List of trait requirements, seperated by commas}

# Definition

+ this is a required method (rk: anything here is displayed as inline code)
* this is an optional method (rk: anything here is displayed as inline code) 

# Implementation

Optional: write `Auto trait.` if it is an auto trait.

Optional: write `Can be derived.` if it can be derived.

Write your rust code here within a markdown block
```

# Haskell Cheatsheet template

```markdown
# TL;DR

Quick summary.

# Requirements

{List of typeclass requirements, separated by commas}

# Definition

+ this is a required method (rk: anything here is displayed as inline code)
* this is an optional method (rk: anything here is displayed as inline code)

# Laws

* Law name: `law in pseudo-haskell`

# Implementation

Write your haskell code here within a markdown block
```

# Architecture

The chetsheets are stored as markdown in the `content` folder (there's a `rust` folder for traits and a `haskell` folder for typeclasses). Inside each of the `rust` and `haskell` folders, traits/typeclasses are grouped together in folders whose name is the name of the group/bag. These bags correspond to traits that are related to each other and are displayed on the homepage.

The cheatsheets are turned into HTML files thanks to a tool developed in Rust, which lies in the `src` folder. The principle is to parse the `content` folder with a custom parser created with `pest`, use an intermediate Rusty representation and then produce the HTML code based on this representation.

The `main.rs` starts by creating the build folder and copying assets to the build directory. Then for each language we generate the HTML cheatsheets. The code for generating traits cheatsheets is in the `rust` folder, and the one for typeclasses is in the `haskell` folder.

In each of these folders, the principle is to have a `build_dir()` in the root which will parse the contents of `content/{language}` and create the corresponding `build/{langage}` output. To do so, the parser in `parser.rs` parses the MD file and fetches the different sections, which are all bundled in a `MdFile` instance (so a `MdFile` is just a structure of parsed sections and other stuff). The parser is automatically generated with `pest` and the grammar can be found in `grammar.pest`. Afterwards, we create HTML sections with the help of the HTML sections generators. These generators are the others Rust files in the `src/{langage}` folder. There is one per section, and it is given as input a raw (MD) section content and outputs the corresponding HTML section. All HTML generators implement `HTMLable`, which is a trait for structures can be outputed to HTML.

To generate HTML files, we get the corresponding template from the `template` folder, replace the placeholders in the template with the output of each section thanks to our HTML generators, and store it in the right file. Placeholders in the template file are of the form `{{var}}`.

In the `template` folder, there are template files and assets. Anything in `assets` will be copied to the output folder directly.